WITH d_meas_tim_rule_day_mtx AS (SELECT
        SRC.MEAS_TIM_RULE_CDE
      , SRC.MEAS_DT
      , SRC.DAY_KEY
      , SRC.WK_KEY
      , SRC.YR_KEY
      , SRC.WK_SEQ
      , SRC.MTH_SEQ
      , SRC.LOC_COMP_STR_REL_DT
      FROM DV_DM_D_MEAS_TIM_RULE_DAY_MTX SRC


  -- Joining for EOP at the day level
    JOIN (SELECT MEAS_TIM_RULE_CDE
            , MIN(DAY_KEY) AS MIN_DAY_KEY
            , MAX(DAY_KEY) AS MAX_DAY_KEY
            , WK_KEY
          FROM DV_DM_D_MEAS_TIM_RULE_DAY_MTX
          WHERE ((( day_key ) >= ((TO_DATE(DATEADD('day', -91, TO_DATE(DATEADD('day', (0 - EXTRACT(DOW FROM DATE_TRUNC('day', CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ))))::integer), DATE_TRUNC('day', CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ))))))))) AND ( day_key ) < ((TO_DATE(DATEADD('day', 91, DATEADD('day', -91, TO_DATE(DATEADD('day', (0 - EXTRACT(DOW FROM DATE_TRUNC('day', CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ))))::integer), DATE_TRUNC('day', CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ))))))))))) 
          OR (( day_key ) >= (TO_DATE(TO_TIMESTAMP('2019-03-03'))) 
          AND ( day_key ) < (TO_DATE(TO_TIMESTAMP('2020-03-01')))))
    and  1=1 -- no filter on 'd_tim_wk_lu.wk'
    and  1=1 -- no filter on 'd_tim_mth_lu.mth'
    and  1=1 -- no filter on 'd_tim_qtr_lu.qtr'
    and  1=1 -- no filter on 'd_tim_halfyr_lu.halfyr'
    and  1=1 -- no filter on 'd_tim_yr_lu.yr'
    and 1=1 -- no filter on 'd_curr_tim_lu_ydy.yesterday_dt'
    and 1=1 -- no filter on 'd_curr_tim_lu_lw.last_week_filter'
   GROUP BY MEAS_TIM_RULE_CDE,WK_KEY) TGT
    ON SRC.MEAS_TIM_RULE_CDE = TGT.MEAS_TIM_RULE_CDE
    AND SRC.WK_KEY = TGT.WK_KEY
    AND (SRC.MEAS_TIM_RULE_CASE = 'AGG')
    OR  (SRC.DAY_KEY = TGT.MIN_DAY_KEY AND SRC.MEAS_TIM_RULE_CASE = 'BOP')
    OR  (SRC.DAY_KEY = TGT.MAX_DAY_KEY AND SRC.MEAS_TIM_RULE_CASE = 'EOP'))
)
SELECT
	--d_tim_wk_lu."WK_KEY"  AS "d_tim_wk_lu.wk",
	--d_org_chn_lu."CHN_ID_DESC"  AS "d_org_chn_lu.chn_id_desc",
	--d_org_loc_lu."LOC_ID"  AS "d_org_loc_lu.loc_id",
	--d_tim_yr_lu."YR_KEY"  AS "d_tim_yr_lu.yr",
	--TO_CHAR(TO_DATE(d_tim_wk_lu."WK_END_DT" ), 'YYYY-MM-DD') AS "d_tim_wk_lu.wk_end_dt",
	--1  AS "MAIN.is_oc_meas",
	--1  AS "MAIN.meas_is_sls",
	--1  AS "MAIN.is_meas",
	--1  AS "MAIN.meas_is_eoh",
	--1  AS "MAIN.meas_is_gl_sls",
	--COALESCE(SUM(CASE WHEN ((MAIN."MEAS_CDE") = 'SLS') THEN 
        --MAIN."F_SLS_RTL"  ELSE NULL END), 0) AS "MAIN.sls_rtl",

	--COALESCE(SUM(CASE WHEN ((MAIN."MEAS_CDE") = 'SLS') THEN 
        --MAIN."F_SLS_QTY"  ELSE NULL END), 0) AS "MAIN.sls_qty",

	--(COALESCE(SUM(CASE WHEN ((MAIN."MEAS_CDE") = 'SLS') THEN
        -- MAIN."F_SLS_RTL"  ELSE NULL END), 0))
        -- - (COALESCE(SUM(CASE WHEN ((MAIN."MEAS_CDE") = 'SLS') THEN MAIN."F_SLS_CST"  ELSE NULL END), 0))  AS "MAIN.gm_amt",

	--COALESCE(SUM(CASE WHEN ((MAIN."MEAS_CDE") = 'EOH') THEN 
        --MAIN."F_EOH_QTY"  ELSE NULL END), 0) AS "MAIN.eoh_qty",

	--COALESCE(SUM(CASE WHEN ((MAIN."MEAS_CDE") = 'EOH') THEN
        --MAIN."F_EOH_CST"  ELSE NULL END), 0) AS "MAIN.eoh_cst",

--COUNT(DISTINCT CASE WHEN ((MAIN."MEAS_CDE") = 'EOH') THEN
--CASE WHEN MAIN."F_MEAS_COL1" > 0 AND MAIN."ORD_BACKORDERED_DT" >= (d_meas_tim_rule_day_mtx."LOC_COMP_STR_REL_DT")
--THEN (d_prd_itm_lu."STY_ID_DESC") END  ELSE NULL END) AS "MAIN.style_count",

--IFNULL((COALESCE(SUM(CASE WHEN ((MAIN."RTL_TYP_CDE") = 'R') THEN
--CASE WHEN (MAIN."MEAS_CDE")='SLS' THEN 
--MAIN."F_MEAS_RTL" END ELSE NULL END), 0)),0)
--+IFNULL((COALESCE(SUM(CASE WHEN ((MAIN."RTL_TYP_CDE") = 'P') THEN 
--CASE WHEN (MAIN."MEAS_CDE")='SLS'
--THEN MAIN."F_MEAS_RTL" END ELSE NULL END), 0)),0) AS "MAIN.regpro_rtl_sls_1",

--(COALESCE(SUM(CASE WHEN (((MAIN."RTL_TYP_CDE") = 'P')) AND (((MAIN."MEAS_CDE") = 'SLS')) THEN 
--MAIN."F_SLS_QTY"  ELSE NULL END), 0))
--+(COALESCE(SUM(CASE WHEN (((MAIN."RTL_TYP_CDE") = 'R')) AND (((MAIN."MEAS_CDE") = 'SLS')) THEN MAIN."F_SLS_QTY"  ELSE NULL END), 0))  AS "MAIN.regpro_u_sls",

--IFNULL((COALESCE(SUM(CASE WHEN (((MAIN."RTL_TYP_CDE") = 'P')) AND
-- (((MAIN."MEAS_CDE") = 'SLS')) 
-- THEN MAIN."F_SLS_RTL" - MAIN."F_SLS_CST"  ELSE NULL END), 0)),0)
-- +IFNULL((COALESCE(SUM(CASE WHEN (((MAIN."RTL_TYP_CDE") = 'R')) AND
--(((MAIN."MEAS_CDE") = 'SLS')) 
--THEN MAIN."F_SLS_RTL"- MAIN."F_SLS_CST" ELSE NULL END), 0)),0)  AS "MAIN.regpro_gm_1",

COALESCE(SUM(CASE WHEN (((MAIN."ITMLOC_STTS_CDE") = 'R')) AND (((MAIN."MEAS_CDE") = 'EOH')) THEN MAIN."F_EOH_QTY"  ELSE NULL END), 0) AS "MAIN.reg_eoh_qty",

COALESCE(SUM(CASE WHEN (((MAIN."ITMLOC_STTS_CDE") = 'R')) AND (((MAIN."MEAS_CDE") = 'EOH')) THEN MAIN."F_EOH_CST"  ELSE NULL END), 0) AS "MAIN.reg_eoh_cst",
COUNT(DISTINCT CASE WHEN (((MAIN."MEAS_CDE") = 'EOH')) AND (((MAIN."ITMLOC_STTS_CDE") = 'R')) THEN CASE WHEN MAIN."F_MEAS_COL1" > 0 AND MAIN."ORD_BACKORDERED_DT" >= (d_meas_tim_rule_day_mtx."LOC_COMP_STR_REL_DT")
            THEN (d_prd_itm_lu."STY_ID_DESC")
        END  ELSE NULL END) AS "MAIN.style_count_reg_price",
	COALESCE(SUM(CASE WHEN (((MAIN."MEAS_CDE") = 'GL_SLS')) AND (NOT (d_prd_itm_lu."ITM_KEY"  IS NULL)) AND (NOT (d_org_loc_lu."LOC_KEY"  IS NULL)) THEN MAIN."F_MEAS_RTL"  ELSE NULL END), 0) AS "MAIN.gl_sls_rtl"
FROM DV_DM_F_MEAS_CCY_IL_B AS MAIN

--INNER JOIN d_meas_tim_rule_day_mtx 
--ON  (d_meas_tim_rule_day_mtx."MEAS_DT") = (MAIN."MEAS_DT")
--and (d_meas_tim_rule_day_mtx."MEAS_TIM_RULE_CDE") = (MAIN."MEAS_TIM_RULE_CDE")

--INNER JOIN DV_DWH_D_TIM_DAY_LU  AS d_tim_day_lu 
--    ON d_meas_tim_rule_day_mtx."DAY_KEY"  = d_tim_day_lu."DAY_KEY"

--INNER JOIN DV_DWH_D_TIM_WK_LU AS d_tim_wk_lu 
--    ON d_meas_tim_rule_day_mtx."WK_KEY"  = d_tim_wk_lu."WK_KEY"

--INNER JOIN DV_DWH_D_TIM_YR_LU   AS d_tim_yr_lu 
--    ON d_meas_tim_rule_day_mtx."YR_KEY"  = d_tim_yr_lu."YR_KEY"

--INNER JOIN DV_DWH_D_ORG_LOC_LU  AS d_org_loc_lu 
--    ON MAIN."LOC_KEY"  = d_org_loc_lu."LOC_KEY"

--INNER JOIN DV_DWH_D_PRD_ITM_LU  AS d_prd_itm_lu 
--    ON MAIN."ITM_KEY"  = d_prd_itm_lu."ITM_KEY"

--LEFT JOIN DV_DWH_D_ORG_CHN_LU  AS d_org_chn_lu 
--    ON MAIN."CHN_KEY"  = d_org_chn_lu."CHN_KEY"

--WHERE ((((d_tim_day_lu."DAY_KEY" ) >= ((TO_DATE(DATEADD('day', -91, TO_DATE(DATEADD('day', (0 - EXTRACT(DOW FROM DATE_TRUNC('day', CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ))))::integer), DATE_TRUNC('day', CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ)))))))
                                        )) 
--AND (d_tim_day_lu."DAY_KEY" ) < ((TO_DATE(DATEADD('day', 91, DATEADD('day', -91, TO_DATE(DATEADD('day', (0 - EXTRACT(DOW FROM DATE_TRUNC('day', CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ))))::integer), DATE_TRUNC('day', CONVERT_TIMEZONE('UTC', 'America/Los_Angeles', CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ))))))))))) 
--OR ((d_tim_day_lu."DAY_KEY" ) >= (TO_DATE(TO_TIMESTAMP('2019-03-03'))) 
--AND (d_tim_day_lu."DAY_KEY" ) < (TO_DATE(TO_TIMESTAMP('2020-03-01'))))))
--MOVING THIS TO WHITH STATEMENT
--AND (d_prd_itm_lu."CLS_ID_DESC") = '0579. HAIR'))

--AND (d_org_chn_lu."CHN_ID_DESC") = '01. Hot Topic')) 

A--ND (MAIN."MEAS_CDE") in (NULL,'EOH','GL_SLS','SLS','EOH')

--and (MAIN."FACT_CDE") in (NULL,210,-135,110,210)
--
GROUP BY 1,2,3,4,TO_DATE(d_tim_wk_lu."WK_END_DT" ),6,7,8,9,10

ORDER BY 1

LIMIT 30